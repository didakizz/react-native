import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import { StatusBar, ActivityIndicator, View, StyleSheet } from 'react-native';
import { GamesScene } from './components/GamesScene'
import { MyWebViewScene } from './components/WebViewScene';


const App = StackNavigator({
  GamesScene: { screen: GamesScene },
  PlayGame: { screen: MyWebViewScene },
});

export default class NavigatorIOSApp extends Component {

  render() {
    StatusBar.setBarStyle('light-content', true);
    return (
      <App style={{ backgroundColor: '#2b2b2b'}}/>
    );
  }
}

