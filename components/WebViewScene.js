import React, { Component } from 'react';
import { Text, NavigatorIOS, WebView } from 'react-native';

export class MyWebViewScene extends Component {

    static navigationOptions = {
      title: 'Playing',
      headerTitleStyle: {
        color: '#EB8A33'
      },
      headerStyle: {
        backgroundColor: '#2b2b2b'
      },
      headerTintColor: '#EB8A33'
    };

    render() {
      const { params } = this.props.navigation.state;

      return (
        <WebView style={{flex: 1}}
          source={{uri: `https://devplay.casino.com/igaming/${params.gamesCode}/`}}
        />
      );
    }
}