import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { ScrollView, ActivityIndicator,
  ListView, Text, View, Image, StyleSheet, NavigatorIOS,
  TouchableHighlight, Button, Dimensions,
  NativeModules, LayoutAnimation } from 'react-native';
import { MyWebViewScene } from './WebViewScene';

// Networking consts
const baseURL = 'https://apiuat.casino.com/lobby/gamefeeds/au/mobile/notflix/?env=stg';

// Android set-up for animations
const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

// Global vars 
let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
let categories = [];

export class GamesScene extends Component { 

  static navigationOptions = {
    title: 'Games',
    headerTitleStyle: {
      color: '#EB8A33'
   },
   headerStyle: {
     backgroundColor: '#2b2b2b'
   },
  };

    constructor(props) {
      super(props);
      this.state = {
        isLoading: true,
        size: Dimensions.get('screen').width / 3,
        currentCategoryIndex: 0
      }
    }

    componentDidMount() {
      this.fetchData()
     }

    // Functions
    onLayout(e) {
      this.setState({
        size: Dimensions.get('screen').width / 3
      })
      // LayoutAnimation.spring();
    }

    onForward(navigate, gamesCode, gameName) {
      navigate('PlayGame', { gamesCode: gamesCode, gameName: gameName })
    }

    switchGame(id) {
      LayoutAnimation.spring();
      
      this.setState({
        dataSource: ds.cloneWithRows(this.categories[id].games),
        currentCategoryIndex: id
      })
    }
    getCategoryName(id) {
      return this.categories[id].name
    }
    
    getCategoryGameButtonViews() {
      var views = [];
      for (let index = 0; index < this.categories.length; index++) {
        views.push(
        <View key={index} style={[styles.categoryButton,
          // update button bgcolor on tap
         { backgroundColor: this.state.currentCategoryIndex == index ? '#EB8A33' : '#2b2b2b' }]}>
    
          <Button title={`${this.getCategoryName(index)} games`} 
          color= {this.state.currentCategoryIndex == index ? '#2b2b2b' : '#EB8A33'} // update button bgcolor on tap
          onPress={() => {this.switchGame(index)}}>
          </Button>

        </View>)
        }
        return views;
    }

    // Networking
    fetchData() {
      return fetch(baseURL)
      .then((response) => response.json())
      .then((responseJson) => {
        // Update categories from responseJson
        this.categories = responseJson.feed.category;
        this.setState({
          isLoading: false,
          gamesCode: this.categories[0].games.game_code,
          dataSource: ds.cloneWithRows(this.categories[0].games),
        });
      })
      .catch((error) => {
        console.error(error);
      });
    }
  
    // Rendering
    render() {
      const { navigate } = this.props.navigation;
      const { params } = this.props.navigation.state;

      if (this.state.isLoading) {
        return (
          <View style={styles.activity}>
            <ActivityIndicator size='large' color='#2b2b2b'/>
          </View>
        );
      }

      return (
        <View style={styles.mainView} onLayout={this.onLayout.bind(this)}> 
            <View style={styles.categoryButtonsContainer}>
                 {this.getCategoryGameButtonViews()}
            </View>
          <ScrollView>
          <ListView key={this.state.size} // key need to update rows on size change
            contentContainerStyle={styles.list}
            dataSource={this.state.dataSource}
            renderRow={(rowData) => 
              <View style={[styles.itemContainer, {width: this.state.size - 5, height: this.state.size}]}>
                  <Text adjustsFontSizeToFit 
                  numberOfLines={1}
                  style={styles.itemText}>{rowData.name}</Text>
                  <TouchableHighlight style={styles.itemTouchable} onPress={() => this.onForward(navigate, rowData.game_code, rowData.name)}>
                  <Image style={styles.itemImage} source={{uri: `https://cache.mansion.com/shared/lobby/web/games/251x147/${rowData.game_code}.jpg`}}/>
                  </TouchableHighlight>
              </View>
              }
          />
          </ScrollView>
        </View>
      );
    }
}

// Styles
const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: '#2b2b2b'
  },
  activity: {
    flex: 1,
    justifyContent: 'center',
  },
  list: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 5,
  },
  itemContainer: {
    padding: 10,
  },
  itemText: {
    textAlign: 'center',
    color: '#EB8A33',
    flex: 0.2
  },
  itemTouchable: {
    flex: 0.8
  },
  itemImage: {
    flex: 1
  },
  categoryButtonsContainer: {
    flex: 0.15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center' 
  },
  categoryButton: {
    flex: 1,
    margin: 1,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#EB8A33'
  }
});